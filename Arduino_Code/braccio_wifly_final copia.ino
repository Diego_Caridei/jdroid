#include "WiFly.h"
#include <Servo.h> 
#define SERIAL_SPEED 9600

int pinFotoRes = A0;
int sensorValue = 0;
int luce_ok = 150;

int auto_pilota = 0;

int pinPiezo = 3;
int durata = 100;

Servo servo_uno;
Servo servo_due;
Servo servo_tre;
Servo servo_quattro;
Servo servo_cinque;
Servo servo_sei;
Servo servo_mobile;

void setup() {
  Serial.begin(SERIAL_SPEED);
  SpiSerial.begin();

  servo_mobile.attach(31);
  servo_uno.attach(33);
  servo_due.attach(35);
  servo_tre.attach(37);
  servo_quattro.attach(39);
  servo_cinque.attach(41);
  servo_sei.attach(43);

  pinMode(pinFotoRes,INPUT);
  pinMode(pinPiezo, OUTPUT);
}


String msg;
String servoStr,posStr;
int k = 0;
int servo,pos;

int n_cicli = 0;
int cicli_remoti = 0;

char buffer[100];

void loop() {
  int incomingByte;
  while(SpiSerial.available() > 0){
    incomingByte = SpiSerial.read() ; // leggo un byte

    if(incomingByte == 42){ // *
      k=0;
      servoStr = "";
      posStr = "";
    }
    else if (incomingByte == 35){ // #

      servo = servoStr.toInt();
      pos = posStr.toInt();

      Serial.print(servo);
      Serial.print(",");
      Serial.println(pos);

    }
    else{
      msg = "";
      msg += char(incomingByte);

      if (incomingByte == 44){ // ,
        k++;
      }
      else{
        if(k == 0){
          //servoStr
          servoStr = servoStr + msg;
        }
        else{
          //posStr
          posStr = posStr + msg;
        }
      }
    }
  }


  //setHome();
  //delay(500);

  sensorValue = analogRead(pinFotoRes);
  Serial.print("luce_ok: ");
  Serial.println(sensorValue);

  if (servo == 0 && pos == 0){
    //vai ad HOME
    setHome();
  }

  if (servo == 200 && pos == 1){
    // abilito autopilota
    auto_pilota = 1;
     Serial.println("abilito auto-pilota");
  }
  else if (servo == 200 && pos == 0){
    //disabilito auto_pilota 
    auto_pilota == 0;
     Serial.println("dis auto-pilota");
  }
  if (servo == 200 && pos > 180){
    //imposto i cicli da remoto
    cicli_remoti = pos-180;
    Serial.println("sono in cicli remoti");
  }

  if ((servo > 0 && servo <= 6) && (pos >= 0 && pos <= 180)){
    //manuale
    manuale (servo,pos);
  }

  if(sensorValue < luce_ok &&  auto_pilota == 1 && n_cicli <= cicli_remoti){
    //oggetto trovato
    beep(3000);
    andata();
    ritorno();
    beep(3000);
    n_cicli++;
  }
  else if (sensorValue > luce_ok && auto_pilota == 1){
    //se non vede l'oggetto suona
    for (int i= 0; i<3; i++){
      beep(durata);
      delay(200);
    }
  }

}

void beep(int delayms){
  analogWrite(pinPiezo, 20);      // Almost any value can be used except 0 and 255
  // experiment to get the best tone
  delay(delayms);          // wait for a delayms ms
  analogWrite(pinPiezo, 0);       // 0 turns it off
}

void setHome(){

  servo_mobile.write(5);

  /* VAI HOME */

  cicla(servo_uno,20,40);
  cicla(servo_due,120,90);
  cicla(servo_tre,120,80);
  cicla(servo_quattro,60,90);
  cicla(servo_cinque,90,100);
  cicla(servo_sei,5,160);
  /*----------*/
}

void andata(){
  /* PRENDI */
  for(int i=5;i<=5;i++){
    servo_sei.write(i);
    delay(20);
  }
  for(int i=40;i<=45;i++){
    servo_uno.write(i);
    delay(20);
  }
  for(int i=90;i<=90;i++){
    servo_due.write(i);
    delay(20);
  }
  for(int i=80;i<=95;i++){
    servo_tre.write(i);
    delay(20);
  }
  for(int i=90;i>=15;i--){
    servo_quattro.write(i);
    delay(20);
  }
  for(int i=90;i>=80;i--){
    servo_cinque.write(i);
    delay(20);
  }
  for(int i=95;i<=120;i++){
    servo_tre.write(i);
    delay(20);
  }
  for(int i=90;i>=88;i--){
    servo_due.write(i);
    delay(20);
  }
  for(int i=5;i<=120;i++){
    servo_sei.write(i);
    delay(20);
  }

  /* SPOSTA */

  for(int i=117;i>=50;i--){
    servo_tre.write(i);
    delay(20);
  }
  servo_mobile.write(90);

  for(int i=45;i<=98;i++){
    servo_uno.write(i);
    delay(20);
  }
  for(int i=90;i<=100;i++){
    servo_due.write(i);
    delay(20);
  }

  for(int i=80;i<=175;i++){
    servo_cinque.write(i);
    delay(20);
  }
  for(int i=10;i>=3;i--){
    servo_quattro.write(i);
    delay(20);
  }

  for(int i=50;i<=105;i++){
    servo_tre.write(i);
    delay(20);
  }
  for(int i=120;i>=5;i--){
    servo_sei.write(i);
    delay(20);
  }

  for(int i=95;i>=70;i--){
    servo_tre.write(i);
    delay(20);
  }

  servo_mobile.write(160);
  delay(500);
  //servo_mobile.write(90);
}

void ritorno(){
  for (int i=95;i<=113;i++){
    servo_uno.write(i);
    delay(20);
  }
  for(int i=3;i<=6;i++){
    servo_quattro.write(i);
    delay(20);
  }
  for(int i=175;i>=90;i--){
    servo_cinque.write(i);
    delay(20);
  }
  for(int i=100;i<=110;i++){
    servo_due.write(i);
    delay(20);

  }
  for(int i=90;i<=110;i++){
    servo_cinque.write(i);
    delay(20);
  }
  for(int i=70;i<=121;i++){
    servo_tre.write(i);
    delay(20);

  }

  for(int i=5;i<=120;i++){
    servo_sei.write(i);
    delay(20);
  }
  for(int i=121;i>=70;i--){
    servo_tre.write(i);
    delay(20);
  }


  for(int i=80;i<=90;i++){
    servo_cinque.write(i);
    delay(20);
  }
  for(int i=95;i>=45;i--){
    servo_uno.write(i);
    delay(20);
  }
  for(int i=100;i>=85;i--){
    servo_due.write(i);
    delay(20);
  }
  for(int i=6;i<=13;i++){
    servo_quattro.write(i);
    delay(20);
  }
  for (int i=70;i<=115;i++){
    servo_tre.write(i);
    delay(20);
  }

  for(int i=120;i>=5;i--){
    servo_sei.write(i);
    delay(20);
  }

  for(int i=13;i<=90;i++){
    servo_quattro.write(i);
    delay(20);
  }
}

void manuale (int numServo, int pos){
  Servo servoMotore;
  Serial.print(numServo);
  Serial.print("-");
  Serial.println(pos);

  if (numServo == 1){
    servoMotore = servo_uno;
  }
  if (numServo == 2){
    servoMotore = servo_due;
  }
  if (numServo == 3){
    servoMotore = servo_tre;
  }
  if (numServo == 4){
    servoMotore = servo_quattro;
  }
  if (numServo == 5){
    servoMotore = servo_cinque;
  }
  if (numServo == 6){
    servoMotore = servo_sei;
  }
  if(numServo == 7){
    servoMotore = servo_mobile;
  }

  servoMotore.write(pos);
  delay(100);

}

void cicla(Servo servoMotore, int inizio, int fine){
  if (inizio > fine){
    for(int i=inizio;i<=fine;i++){
      servoMotore.write(i);
      delay(20);
    }
  }
  else{
    for(int i=fine;i>=inizio;i--){
      servoMotore.write(i);
      delay(20);
    }
  }
  delay(100);
}




