# JDroid KIT
## Robot arm + mouse controller
![jdroid_arm.jpg](https://bitbucket.org/repo/4odyn4/images/4255154676-jdroid_arm.jpg)

JDroid is a system that connects different modules necessary for the treatment of the elements from arrival to the validation of the work cycles.

Check the folder "relazioni" all further information, news and updates on functions.


### FreeStyle
* Control JDroid, with your **mouse**!
* **Fluid** movements from your arm to the robotic one choosing the desired **articulation**.
* **COMBO** feature! Many joints and **gripper** work simultaneously!

### Let me do this

* Set the work cycles, the rest let JDroid think. At the end of the execution a beep indicates the success!
* With JDroid you can process all of our items with a single, consolidated and automated technological system.

### PRO
* **Evolution**: A true custom system and evolutionary.
* **Flexibility**: Suitable for configuration and flexibility of your goal.
* **Freedom**: Ability to complete actions in different ways.

---
Software/Hardware developed by Diego Caridei and  Fabio Nisci.