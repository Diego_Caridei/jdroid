/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jdroid_wifi;

import java.awt.Image;
import java.awt.Toolkit;

/**
 *
 * @author Diego Caridei, Fabio Nisci
 */
public class JDroid_wifi {
    
    //subnet in cui cercare il wifly già connesso
    public static final String SUBNET = "192.168.0";

    public static void main(String[] args) throws Exception {
        MainForm form = new MainForm();

        //imposta il form al centro dello schermo
        form.setSize(900, 380);
        form.pack();
        form.setLocationRelativeTo(null);
        //---

        //rende il form visibile  e focusable
        form.setVisible(true);
        form.setFocusable(true);
        
        //aggiorna gli host raggiungibili        
        form.checkHosts();
        
    }
}
