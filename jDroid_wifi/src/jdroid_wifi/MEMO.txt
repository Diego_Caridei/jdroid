PROMEMORIA COMANDI

POSIZIONE
servo, posizione -> servo da 1 a 6 | posizione da 0 a 180
0,0 -> set braccio a HOME
100,valore -> set luce_ok a valore rilevato con oggetto in posizione (coperto)
200,1 -> attiva il pilota automatico
200,0 -> disattiva il pilota automatico
200,n -> invia n numero di cicli da eseguire

 