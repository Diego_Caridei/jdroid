/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jdroid_wifi;

import java.awt.Component;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.ListIterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JToggleButton;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 *
 * @author Diego Caridei, Fabio Nisci
 */

public class MainForm extends javax.swing.JFrame implements MouseMotionListener, MouseListener, MouseWheelListener {

    
    
    
    //controlli
    int servoManuale = -1;
    int manualCtrl = -1;
    int combo_mov = -1;
    int gripper_pos = 90;
    ArrayList servoLbl;

    /**
     * Creates new form MainForm
     */
    public MainForm() throws Exception {
        initComponents();

        servoManuale = 1;
        combo_mov = 1;
        manualCtrl = 0;

        //aggiungo i listener mouse all'area di controllo
        blankArea.addMouseWheelListener(this);//rotellina
        blankArea.addMouseListener(this);//click
        blankArea.addMouseMotionListener(this);//moviemento

        //label che contengono i valori dei servo per i preset
        servoLbl = new ArrayList();
        servoLbl.add(tronco_value);
        servoLbl.add(spalla_value);
        servoLbl.add(gomito_value);
        servoLbl.add(polso_up_down_value);
        servoLbl.add(polso_dx_sx_value);
        servoLbl.add(gripper_value);

        //avvio il thread per ricevere di continuo
        ricevi.start();
    }

    /*
     * aggiorna il combo box con gli host connessi e raggiungibili
     * per la connessione.
     */
    public void checkHosts() {
        String subnet = JDroid_wifi.SUBNET; //subnet da controllare
        ip_remoto_combo.setEnabled(Boolean.FALSE);
        ip_remoto_combo.removeAllItems();
        ip_remoto_combo.addItem("127.0.0.1"); //inserisco il localhost per i test
        int timeout = 10;
        for (int i = 1; i < 254; i++) {
            try {
                String host = subnet + "." + i;

                if (InetAddress.getByName(host).isReachable(timeout)) {
                    //se l'host è raggiungibile lo aggiungo
                    ip_remoto_combo.addItem(host);
                    stampa(host + " is reachable");
                }
            } catch (Exception ex) {
                stampa(ex.getMessage());
            }
        }
        stampa("HOST aggiornati");
        ip_remoto_combo.setEnabled(Boolean.TRUE);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        gruppoRadio = new javax.swing.ButtonGroup();
        jOptionPane1 = new javax.swing.JOptionPane();
        info_bar = new javax.swing.JLabel();
        blankArea = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        tronco_radio = new javax.swing.JRadioButton();
        spalla_radio = new javax.swing.JRadioButton();
        gomito_radio = new javax.swing.JRadioButton();
        polso_su_giu_radio = new javax.swing.JRadioButton();
        polso_sx_dx_radio = new javax.swing.JRadioButton();
        gripper_radio = new javax.swing.JRadioButton();
        gripper_value = new javax.swing.JLabel();
        gomito_value = new javax.swing.JLabel();
        polso_up_down_value = new javax.swing.JLabel();
        polso_dx_sx_value = new javax.swing.JLabel();
        tronco_value = new javax.swing.JLabel();
        spalla_value = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        response_txt = new javax.swing.JTextArea();
        invia_txt = new javax.swing.JTextField();
        invia_btn = new javax.swing.JButton();
        auto_pilota_toggle = new javax.swing.JToggleButton();
        ip_remoto_combo = new javax.swing.JComboBox();
        refresh_host_btn = new javax.swing.JButton();
        save_preset_btn = new javax.swing.JButton();
        run_preset_btn = new javax.swing.JButton();
        n_cicli = new javax.swing.JTextField();
        clear_btn = new javax.swing.JButton();
        sfondo = new javax.swing.JLabel();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        about_menuItem = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("JDroid");
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        info_bar.setBackground(new java.awt.Color(0, 0, 0));
        info_bar.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        info_bar.setText("JDroid Avviato... ricevo sulla porta 9000 | invio sulla porta 8000");
        info_bar.setToolTipText("");
        getContentPane().add(info_bar, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 630, 410, -1));

        blankArea.setBackground(new java.awt.Color(255, 255, 204));
        blankArea.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        blankArea.setPreferredSize(new java.awt.Dimension(180, 180));

        org.jdesktop.layout.GroupLayout blankAreaLayout = new org.jdesktop.layout.GroupLayout(blankArea);
        blankArea.setLayout(blankAreaLayout);
        blankAreaLayout.setHorizontalGroup(
            blankAreaLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 190, Short.MAX_VALUE)
        );
        blankAreaLayout.setVerticalGroup(
            blankAreaLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 178, Short.MAX_VALUE)
        );

        getContentPane().add(blankArea, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 20, 192, -1));

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setPreferredSize(new java.awt.Dimension(180, 180));
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/jdroid_wifi/robotArm.jpeg"))); // NOI18N
        jPanel1.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(6, 6, -1, 143));

        gruppoRadio.add(tronco_radio);
        tronco_radio.setSelected(true);
        tronco_radio.setText("1 - Tronco");
        tronco_radio.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                tronco_radioStateChanged(evt);
            }
        });
        jPanel1.add(tronco_radio, new org.netbeans.lib.awtextra.AbsoluteConstraints(6, 190, -1, -1));

        gruppoRadio.add(spalla_radio);
        spalla_radio.setText("2 - Spalla");
        spalla_radio.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                spalla_radioStateChanged(evt);
            }
        });
        jPanel1.add(spalla_radio, new org.netbeans.lib.awtextra.AbsoluteConstraints(6, 219, -1, -1));

        gruppoRadio.add(gomito_radio);
        gomito_radio.setText("3 - Gomito");
        gomito_radio.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                gomito_radioStateChanged(evt);
            }
        });
        jPanel1.add(gomito_radio, new org.netbeans.lib.awtextra.AbsoluteConstraints(6, 248, -1, -1));

        gruppoRadio.add(polso_su_giu_radio);
        polso_su_giu_radio.setText("4 - Polso su/giù");
        polso_su_giu_radio.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                polso_su_giu_radioStateChanged(evt);
            }
        });
        jPanel1.add(polso_su_giu_radio, new org.netbeans.lib.awtextra.AbsoluteConstraints(6, 277, -1, -1));

        gruppoRadio.add(polso_sx_dx_radio);
        polso_sx_dx_radio.setText("5 - Polso dx/rx");
        polso_sx_dx_radio.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                polso_sx_dx_radioStateChanged(evt);
            }
        });
        jPanel1.add(polso_sx_dx_radio, new org.netbeans.lib.awtextra.AbsoluteConstraints(6, 306, -1, -1));

        gruppoRadio.add(gripper_radio);
        gripper_radio.setText("6 - Gripper");
        gripper_radio.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                gripper_radioStateChanged(evt);
            }
        });
        jPanel1.add(gripper_radio, new org.netbeans.lib.awtextra.AbsoluteConstraints(6, 335, -1, -1));

        gripper_value.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        gripper_value.setText("0");
        jPanel1.add(gripper_value, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 340, 40, -1));

        gomito_value.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        gomito_value.setText("0");
        jPanel1.add(gomito_value, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 250, 40, -1));

        polso_up_down_value.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        polso_up_down_value.setText("0");
        jPanel1.add(polso_up_down_value, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 280, 40, -1));

        polso_dx_sx_value.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        polso_dx_sx_value.setText("0");
        jPanel1.add(polso_dx_sx_value, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 310, 40, -1));

        tronco_value.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        tronco_value.setText("0");
        jPanel1.add(tronco_value, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 190, 40, -1));

        spalla_value.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        spalla_value.setText("0");
        jPanel1.add(spalla_value, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 220, 40, -1));

        getContentPane().add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 220, 190, 370));

        response_txt.setEditable(false);
        response_txt.setColumns(20);
        response_txt.setRows(5);
        response_txt.setToolTipText("Risposta da remoto");
        jScrollPane1.setViewportView(response_txt);

        getContentPane().add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 20, 250, 352));

        invia_txt.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        invia_txt.setToolTipText("Comandi manuali");
        invia_txt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                invia_txtActionPerformed(evt);
            }
        });
        getContentPane().add(invia_txt, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 420, 240, -1));

        invia_btn.setText("Invia");
        invia_btn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                invia_btnActionPerformed(evt);
            }
        });
        getContentPane().add(invia_btn, new org.netbeans.lib.awtextra.AbsoluteConstraints(345, 460, 110, -1));

        auto_pilota_toggle.setText("Auto Pilota");
        auto_pilota_toggle.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                auto_pilota_toggleActionPerformed(evt);
            }
        });
        getContentPane().add(auto_pilota_toggle, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 500, 160, -1));

        ip_remoto_combo.setMaximumRowCount(20);
        getContentPane().add(ip_remoto_combo, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 380, 200, 30));

        refresh_host_btn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/jdroid_wifi/arrow-refresh.png"))); // NOI18N
        refresh_host_btn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                refresh_host_btnActionPerformed(evt);
            }
        });
        getContentPane().add(refresh_host_btn, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 380, 40, 30));

        save_preset_btn.setText("Salva Preset");
        save_preset_btn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                save_preset_btnActionPerformed(evt);
            }
        });
        getContentPane().add(save_preset_btn, new org.netbeans.lib.awtextra.AbsoluteConstraints(230, 540, 120, -1));

        run_preset_btn.setText("Run Preset");
        run_preset_btn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                run_preset_btnActionPerformed(evt);
            }
        });
        getContentPane().add(run_preset_btn, new org.netbeans.lib.awtextra.AbsoluteConstraints(350, 540, 110, -1));

        n_cicli.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        n_cicli.setText("0");
        getContentPane().add(n_cicli, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 500, 70, -1));

        clear_btn.setText("Pulisci");
        clear_btn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                clear_btnActionPerformed(evt);
            }
        });
        getContentPane().add(clear_btn, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 460, 100, -1));

        sfondo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/jdroid_wifi/background.jpg"))); // NOI18N
        sfondo.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);
        getContentPane().add(sfondo, new org.netbeans.lib.awtextra.AbsoluteConstraints(-260, 0, 1020, 650));

        jMenu1.setText("JDroid");

        about_menuItem.setText("Informazioni");
        about_menuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                about_menuItemActionPerformed(evt);
            }
        });
        jMenu1.add(about_menuItem);

        jMenuBar1.add(jMenu1);

        setJMenuBar(jMenuBar1);

        pack();
    }// </editor-fold>//GEN-END:initComponents
    Thread ricevi = new Thread() {
        //THREAD di ricezione messaggi
        public void run() {
            //creo un nuovo datagramma in ricezione
            DatagramSocket ricevitore = null;
            try {
                ricevitore = new DatagramSocket(9000); //ricevo sulla porta 9000
            } catch (SocketException ex) {
                stampa(ex.getMessage());
            }
            while (true) {
                //crea un buffer che conterrà il messaggio
                byte[] buffer = new byte[255];
                //crea il pacchetto che conterrà il buffer (ricevuto) + la lunghezza
                DatagramPacket packet = new DatagramPacket(buffer, buffer.length);
                try {
                    //mi metto in attesa di ricevere un messaggio
                    ricevitore.receive(packet);
                } catch (IOException ex) {
                    stampa(ex.getMessage());
                }
                //estraggo il messaggio dal pacchetto e lo visualizzo nel response_txt
                //messaggio = byte ricevuti, offset e lunghezza del messaggio
                String messaggio = new String(packet.getData(), packet.getOffset(), packet.getLength());
                response_txt.append("<< " + messaggio + "\n");
                //auto scroll per vedere sempre l'ultimo messaggio ricevuto
                response_txt.setCaretPosition(response_txt.getDocument().getLength());
            }
        }
    };

    private void invia_txtActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_invia_txtActionPerformed
        //invia il messaggio anche se premo invio sulla tasiera
        invia_btnActionPerformed(evt);
    }//GEN-LAST:event_invia_txtActionPerformed

    //radioButton
    private void tronco_radioStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_tronco_radioStateChanged
        servoManuale = 1;
        combo_mov = 1;
    }//GEN-LAST:event_tronco_radioStateChanged

    private void spalla_radioStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_spalla_radioStateChanged
        servoManuale = 2;
        combo_mov = 0;
    }//GEN-LAST:event_spalla_radioStateChanged

    private void gomito_radioStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_gomito_radioStateChanged
        servoManuale = 3;
        combo_mov = 0;
    }//GEN-LAST:event_gomito_radioStateChanged

    private void polso_su_giu_radioStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_polso_su_giu_radioStateChanged
        servoManuale = 4;
        combo_mov = 0;
    }//GEN-LAST:event_polso_su_giu_radioStateChanged

    private void polso_sx_dx_radioStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_polso_sx_dx_radioStateChanged
        servoManuale = 5;
        combo_mov = 0;
    }//GEN-LAST:event_polso_sx_dx_radioStateChanged

    private void gripper_radioStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_gripper_radioStateChanged
        servoManuale = 6;
        combo_mov = 0;
    }//GEN-LAST:event_gripper_radioStateChanged

    private void invia_btnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_invia_btnActionPerformed
        //verifico se il text contiene del testo
        if (invia_txt.getText().length() != 0) {
            //parsing di tutti gli "elementi" inseriti in console per darli alla 
            // funzione invio in ordine corretto
            ArrayList<String> list = new ArrayList<String>();
            list.addAll(Arrays.asList(invia_txt.getText().split("\\s*,\\s*")));
            //stampa(list);

            /*
             * "\\s*,\\s*" È un espressione regolare (regex).
             * \s indica gli spazi vuoti
             * quindi \s* significa di adattarsi a tutti (o nessuno) spazi bianchi
             * prendendo tutti i caratteri in considerazione
             * esempio: "item1 , item2 , item3", è uguale a
             * "item1,item2 ,item3"
             */

            try {
                invio(Integer.parseInt(list.get(0)), Integer.parseInt(list.get(1)));
            } catch (Exception e) {
                String message = "Parametri non conformi, correggi!\n";
                JOptionPane.showMessageDialog((Component) null, message, "Errore", JOptionPane.WARNING_MESSAGE);
            }
        }
    }//GEN-LAST:event_invia_btnActionPerformed

    private void auto_pilota_toggleActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_auto_pilota_toggleActionPerformed
        JToggleButton auto_jtoggle = (JToggleButton) evt.getSource();
        boolean selected = auto_jtoggle.getModel().isSelected();

        if (!selected) {
            try {
                //disattiva pilota automatico                
                invio(200, 0);
                //attivo componenti di input
                invia_btn.setEnabled(Boolean.TRUE);
                invia_txt.setEnabled(Boolean.TRUE);
                ip_remoto_combo.setEnabled(Boolean.TRUE);
                blankArea.setVisible(Boolean.TRUE);
                refresh_host_btn.setEnabled(Boolean.TRUE);
            } catch (Exception ex) {
                stampa(ex.getMessage());
            }
        } else {
            try {
                //attiva pilota automatico
                invio(200, 1);
                invio(200, Integer.parseInt(n_cicli.getText()) + 180);
                //disattivo componenti di input
                invia_btn.setEnabled(Boolean.FALSE);
                invia_txt.setEnabled(Boolean.FALSE);
                ip_remoto_combo.setEnabled(Boolean.FALSE);
                blankArea.setVisible(Boolean.FALSE);
                refresh_host_btn.setEnabled(Boolean.FALSE);
            } catch (Exception ex) {
                stampa(ex.getMessage());
            }
        }
    }//GEN-LAST:event_auto_pilota_toggleActionPerformed

    private void about_menuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_about_menuItemActionPerformed
        //mostra schermata di About
        String string = "Applicazione ideata e realizzata da Diego Caridei e Fabio Nisci";
        ImageIcon icon = new ImageIcon(getClass().getResource("about.jpeg"));
        JOptionPane.showMessageDialog(null, string, "JDroid", JOptionPane.INFORMATION_MESSAGE, icon);
    }//GEN-LAST:event_about_menuItemActionPerformed

    private void refresh_host_btnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_refresh_host_btnActionPerformed
        //ricarica gli host connessi disponibili
        checkHosts();
    }//GEN-LAST:event_refresh_host_btnActionPerformed

    private void save_preset_btnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_save_preset_btnActionPerformed
        //apre filechooser
        JFileChooser fc = new JFileChooser(JFileChooser.FILE_FILTER_CHANGED_PROPERTY);
        //filtro file .jdroid
        FileNameExtensionFilter extFilter = new FileNameExtensionFilter("JDroid preset", "jdroid");
        fc.setFileFilter(extFilter);
        fc.setDialogTitle("Salva file di preset JDroid");
        if (fc.showSaveDialog(this) == JFileChooser.APPROVE_OPTION) {
            //se viene cliccato salva, salvo il file
            fileSaver(fc);
        }
    }//GEN-LAST:event_save_preset_btnActionPerformed

    private void fileSaver(JFileChooser fc) {
        //salva la stringa passata dal filechooser
        File file = fc.getSelectedFile();
        // concateno i valori da inserire
        StringBuffer sb = new StringBuffer();

        //salvo tutti i valori dei servo modificati
        for (ListIterator it = servoLbl.listIterator(); it.hasNext();) {
            JLabel CurLbl = (JLabel) it.next();
            if (Integer.parseInt(CurLbl.getText()) > 0) {
                //salvo solo nel caso il valore sia maggiore di zero per non avere movimenti ridondanti
                sb.append(it.nextIndex()).append(",").append(CurLbl.getText()).append("\n");
            }
        }

        String testoDaSalvare = sb.toString();
        if (testoDaSalvare == null) {
            JOptionPane.showMessageDialog(this, "Nessun movimento selezionato", "Attenzione", JOptionPane.WARNING_MESSAGE);
            return;
        }

        BufferedWriter writer = null;
        //controlliamo che l'estensione sia giusta
        String filePath = file.getPath();
        //aggiungo estensione .jdroid se l'utente l'ha dimeticato
        if (!filePath.toLowerCase().endsWith(".jdroid")) {
            file = new File(filePath + ".jdroid");
        }

        try {
            writer = new BufferedWriter(new FileWriter(file));
            writer.write(testoDaSalvare);
            JOptionPane.showMessageDialog(this, "Preset salvato con successo. (" + file.getName() + ")", "Preset salvato", JOptionPane.INFORMATION_MESSAGE);
        } catch (IOException e) {
            JOptionPane.showMessageDialog(this, e.getMessage(), "Errore", JOptionPane.ERROR_MESSAGE);
        } finally {
            try {
                if (writer != null) {
                    writer.close();
                }
            } catch (IOException e) {
                JOptionPane.showMessageDialog(this, e.getMessage(), "Errore", JOptionPane.ERROR_MESSAGE);
            }
        }

    }

    private void run_preset_btnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_run_preset_btnActionPerformed

        JFileChooser fc = new JFileChooser(JFileChooser.FILE_FILTER_CHANGED_PROPERTY);
        fc.setDialogTitle("Apri JDroid preset");
        //imposta filtro per leggere solo i file .jdroid
        FileNameExtensionFilter extFilter = new FileNameExtensionFilter("JDroid preset", "jdroid");
        fc.setFileFilter(extFilter);

        if (fc.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
            //apre il file
            fileOpen(fc);
        }
    }//GEN-LAST:event_run_preset_btnActionPerformed

    private void clear_btnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_clear_btnActionPerformed
        response_txt.setText("");
    }//GEN-LAST:event_clear_btnActionPerformed

    public void fileOpen(JFileChooser fc) {
        //ottiene il file selezionato
        File file = fc.getSelectedFile();
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new FileReader(file));
            String curLine;
            while ((curLine = reader.readLine()) != null) {
                //invio riga per riga i comandi finchè non finisce il file

                //parsing di tutti gli "elementi" riga del file per darli alla 
                // funzione invio in ordine corretto
                ArrayList<String> list = new ArrayList<String>();
                list.addAll(Arrays.asList(curLine.split("\\s*,\\s*")));
                //stampa(list);

                try {
                    invio(Integer.parseInt(list.get(0)), Integer.parseInt(list.get(1)));
                } catch (Exception e) {
                    String message = "Parametri non conformi, correggi!\n";
                    JOptionPane.showMessageDialog((Component) null, message, "Errore", JOptionPane.WARNING_MESSAGE);
                }

                stampa(curLine);//logger
            }
        } catch (IOException e) {
            JOptionPane.showMessageDialog(this, e.getMessage(), "Errore", JOptionPane.ERROR_MESSAGE);
        } finally {
            try {
                if (reader != null) {
                    reader.close();
                }
            } catch (IOException e) {
                JOptionPane.showMessageDialog(this, e.getMessage(), "Errore", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    void stampa(Object qualcosa) {
        //funzione di aiuto per la stampa in console
        System.out.println(qualcosa);
    }

    void invio(int servo, int pos) throws Exception {
        //logger per test
        // stampa("LOG: " + servo + "," + pos);

        //sentence rappresenta la stringa formattata da inviare
        String sentence = "*" + servo + "," + pos + "#";
        //Datagram è l'implementazione java di un pacchetto udp
        DatagramSocket sender = new DatagramSocket();
        // nome host
        String serverHostname = ip_remoto_combo.getSelectedItem().toString();
        //crea l'oggetto indirizzo IP dalla stringa
        InetAddress IPAddress = InetAddress.getByName(serverHostname);
        //Vettore che conterrà l'informazione da inviare
        byte[] sendData = new byte[1024];
        // effettua l’encoding usando il charset della piattaforma su cui gira java
        sendData = sentence.getBytes();
        //creo il pacchetto 
        DatagramPacket sendPacket =
                new DatagramPacket(sendData, sendData.length, IPAddress, 8000);
        //invio il pacchetto
        sender.send(sendPacket);

        response_txt.append(">> " + sentence + "\n");
        //auto scroll per vedere sempre l'ultimo messaggio ricevuto
        response_txt.setCaretPosition(response_txt.getDocument().getLength());
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) throws Exception {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;




                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MainForm.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MainForm.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MainForm.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MainForm.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    new MainForm().setVisible(true);
                } catch (Exception ex) {
                    Logger.getLogger(MainForm.class
                            .getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuItem about_menuItem;
    private javax.swing.JToggleButton auto_pilota_toggle;
    private javax.swing.JPanel blankArea;
    private javax.swing.JButton clear_btn;
    private javax.swing.JRadioButton gomito_radio;
    private javax.swing.JLabel gomito_value;
    private javax.swing.JRadioButton gripper_radio;
    private javax.swing.JLabel gripper_value;
    private javax.swing.ButtonGroup gruppoRadio;
    private javax.swing.JLabel info_bar;
    private javax.swing.JButton invia_btn;
    private javax.swing.JTextField invia_txt;
    private javax.swing.JComboBox ip_remoto_combo;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JOptionPane jOptionPane1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextField n_cicli;
    private javax.swing.JLabel polso_dx_sx_value;
    private javax.swing.JRadioButton polso_su_giu_radio;
    private javax.swing.JRadioButton polso_sx_dx_radio;
    private javax.swing.JLabel polso_up_down_value;
    private javax.swing.JButton refresh_host_btn;
    private javax.swing.JTextArea response_txt;
    private javax.swing.JButton run_preset_btn;
    private javax.swing.JButton save_preset_btn;
    private javax.swing.JLabel sfondo;
    private javax.swing.JRadioButton spalla_radio;
    private javax.swing.JLabel spalla_value;
    private javax.swing.JRadioButton tronco_radio;
    private javax.swing.JLabel tronco_value;
    // End of variables declaration//GEN-END:variables

    @Override
    public void mouseDragged(MouseEvent me) {
    }

    
    @Override
    public void mouseMoved(MouseEvent me) {
        //il mouse si muove ed invia le coordinate di movimento
        //doppio click
        if (manualCtrl == 1) {
            try {
                //invio dati in formato servo,valore
                invio(servoManuale, me.getX());

                JLabel curLbl = (JLabel) servoLbl.get(servoManuale - 1);
                curLbl.setText(Integer.toString(me.getX()));

                if (combo_mov == 1) {
                    invio(3, me.getY());
                    gomito_value.setText(Integer.toString(me.getY()));
                }
            } catch (Exception ex) {
                stampa(ex.getMessage());
            }
        }
    }

    @Override
    public void mouseClicked(MouseEvent me) {
        //comandi manuali
        if (me.getClickCount() == 2) {
            //i comandi manuali iniziano se si fa doppio click
            if (manualCtrl == 0) {
                //abilito controllo se fatto doppio click
                manualCtrl = 1;
            } else if (manualCtrl == 1) {
                manualCtrl = 0;
            }
        }
    }

    @Override
    public void mousePressed(MouseEvent me) {
    }

    @Override
    public void mouseReleased(MouseEvent me) {
    }

    @Override
    public void mouseEntered(MouseEvent me) {
    }

    @Override
    public void mouseExited(MouseEvent me) {
        //interrompere controllo manuale 
        manualCtrl = 0;
    }

    @Override
    public void mouseWheelMoved(MouseWheelEvent mwe) {
        // apri / chiudi gripper
        int notches = mwe.getWheelRotation();
        if (notches < 0) {
            // "Rotellina SU" chiude
            if (gripper_pos > 0) {
                gripper_pos = gripper_pos + notches; //notches negative
            } else {
                gripper_pos = 0;
            }
        } else {
            // "Rotellina GIU" apre
            if (gripper_pos > 180) {
                gripper_pos = gripper_pos + notches; //notches positive
            } else {
                gripper_pos = 180;
            }
        }
        try {
            //invio la posizione del gripper
            invio(6, gripper_pos);
        } catch (Exception ex) {
            stampa(ex.getMessage());
        }
    }
}
